function publicar(){
  var usuario =  localStorage.getItem("usuario");
  var titulo = document.getElementsByName('titulo')[0].value;
  var contenido = document.getElementsByName('contenido')[0].value;
  var imagen = "";
  var documento = "";
  var d = fecha();
  console.log(d);
  if(titulo == "" || contenido == ""){
    uniface.activate('mensaje', 'Complete los campos');
  } else {
    var file = document.getElementById("fileA").files[0];
    var file2 = document.getElementById("fileB").files[0];
    if (!file && !file2) {
      imagen = "";
      documento = "";
      document.getElementById("fileA").value = "";
      uniface.getInstance('CVZLAPUBLICAR').activate('publicar', usuario, titulo, contenido, imagen, d, documento);
      window.history.back();
    } else {
      if(file2 == "" || file2 == null){
        uploadFile3();
      }else{
		  if(file2.type == "text/plain" || file2.type == "application/pdf" || file2.type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document"){
			  uploadFile5();
		  }else{
			  uniface.activate("mensaje", "Tipo de archivo no valido")
		  }
        
      }
    }
  }
}

function uploadFile2(){
  var preview = document.getElementsByClassName('preview')[0];
  var file    = document.querySelector('input[type=file]').files[0]; 
  var reader  = new FileReader();

  reader.onloadend = function () {
    preview.src = reader.result;
  }

  if (file) {
    reader.readAsDataURL(file);
  } else {
    preview.src = "";
  }
}

function uploadFile3(dsp){

  var file = document.getElementById("imagen").files[0];
  if (!file) {
    alert("No file selected!");
    return;
  }
  /*if(file.size >= 1000000){
	uniface.activate("mensaje", "El tamaño del archivo es mayor a 1mb")
	return;
  }*/
  var d = new Date();
  var formData = new FormData();
  client = new XMLHttpRequest();

  formData.append("FILE_UPLOAD.DUMMY.1", file);
  storeFileName = file.name;
  formData.append("UPLOADFILENAME.DUMMY.1", storeFileName);

  client.onerror = function (e) {
    alert("onError");
  };

  client.onload = function (e) {
	  var file = document.getElementById("imagen").files[0];
document.getElementById("imagen").value = "";
publicar2(file.name, dsp);
  };

  client.onabort = function (e) {
    alert("Upload aborted");
  };

  client.open("POST", "FILE_UPLOAD_USPP");
  client.send(formData);
}

function uploadFile4(){

  var file = document.getElementById("imagen").files[0];
  if (!file) {
    alert("No file selected!");
    return;
  }
  if(file.size >= 1000000){
	uniface.activate("mensaje", "El tamaño del archivo es mayor a 1mb")
	return;
  }
  var d = new Date();
  var formData = new FormData();
  client = new XMLHttpRequest();

  formData.append("FILE_UPLOAD.DUMMY.1", file);
  storeFileName = file.name;
  formData.append("UPLOADFILENAME.DUMMY.1", storeFileName);

  client.onerror = function (e) {
    alert("onError");
  };

  client.onload = function (e) {
	  var file = document.getElementById("imagen").files[0];
document.getElementById("imagen").value = "";
publicar3(file.name);
  };

  client.onabort = function (e) {
    alert("Upload aborted");
  };

  client.open("POST", "FILE_UPLOAD_USPP");
  client.send(formData);
}

function uploadFile5(){

  var file = document.getElementById("imagen").files[0];
  if (!file) {
    alert("No file selected!");
    return;
  }
  if(file.size >= 1000000){
	uniface.activate("mensaje", "El tamaño del archivo es mayor a 1mb")
	return;
  }
  var d = new Date();
  var formData = new FormData();
  client = new XMLHttpRequest();

  formData.append("FILE_UPLOAD.DUMMY.1", file);
  storeFileName = file.name;
  formData.append("UPLOADFILENAME.DUMMY.1", storeFileName);

  client.onerror = function (e) {
    alert("onError");
  };

  client.onload = function (e) {
	  var file = document.getElementById("imagen").files[0];
document.getElementById("imagen").value = "";
publicar4(file.name);
  };

  client.onabort = function (e) {
    alert("Upload aborted");
  };

  client.open("POST", "FILE_UPLOAD_USPP");
  client.send(formData);
}

function publicar2(img, dsp){
	var titulo = document.getElementsByClassName('titulo')[0].value;
	var contenido = document.getElementsByClassName('nicEdit-main')["0"].innerHTML;
	var imagen = img;
	var publicacion = document.getElementsByClassName('publicacion')["0"].value;
	var fecha = document.getElementsByClassName('fecha')["0"].value;
	var preview = document.getElementsByClassName('preview')[0].value;
	if(preview != "" && imagen == ""){
		var res = preview.split("/");
		imagen = res[4];
	}
	var res = fecha.split("-");
	var month = new Array();
month[0] = "Ene";
month[1] = "Feb";
month[2] = "Mar";
month[3] = "Abr";
month[4] = "May";
month[5] = "Jun";
month[6] = "Jul";
month[7] = "Ago";
month[8] = "Sep";
month[9] = "Oct";
month[10] = "Nov";
month[11] = "Dic";
var n = month[res[1]-1];
	var fecha = n + "<br><h>"+res[2]+"</h>"
	var estado = document.getElementsByClassName('lista')["0"].value;
	var file = document.getElementById("imagen").files[0];
	
	if (!file){
		uniface.getInstance(dsp).activate("guardar", publicacion, titulo, contenido, imagen, fecha, estado);
	}else{
		uploadFile3(dsp);
	}
}

function publicar3(img){
	var contenido = document.getElementById('contenido').value;
	var imagen = img;
	var estado = document.getElementById("estado").options[document.getElementById("estado").selectedIndex].text;
	var file = document.getElementById("imagen").files[0];
	
	if (!file){
		uniface.getInstance("ADMINSPONSORS").activate("guardar", contenido, imagen, estado);
	}else{
		uploadFile4();
	}
}

function publicar4(img){
	var imagen = img;
	var estado = document.getElementById("estado").options[document.getElementById("estado").selectedIndex].text;
	var file = document.getElementById("imagen").files[0];
	
	if (!file){
		uniface.getInstance("ADMINGALERIA").activate("guardar", imagen, estado);
	}else{
		uploadFile5();
	}
}