var videos = 1;
var uf_clientside = uf_clientside || {};

function inicio(){
	var usuario = localStorage.getItem("usuario");
	var welcome = localStorage.getItem("welcome");

	if(usuario == "" || usuario == null){
		document.body.style.backgroundImage = "url(../css/img/fondovene-min.jpg)";
		document.body.style.backgroundSize = "cover";
		uniface.activate("invalido").catch(function(e) { console.log(e); } );
	} else if(welcome == "true"){
		document.body.style.backgroundImage = "url(../css/img/fondovene-min.jpg)";
		document.body.style.backgroundSize = "cover";
		uniface.activate("bienvenida").catch(function(e) { console.log(e); } );
	} else {
		document.body.style.backgroundImage = "";
		document.body.style.backgroundSize = "";
		uniface.activate("valido", usuario).catch(function(e) { console.log(e); } );
	}
}

window.onhashchange = function() {
	var hash = window.location.hash.substr(1);
	var usuario = localStorage.getItem("usuario");

	if(hash !== ""){
		if(usuario != "" || usuario != null){
			var cual = cuales(hash);
			cambia(cual);
		}
		uniface.activate("cambiaDSP", hash).catch(function(e) { console.log(e); } );
	} else {
		if(usuario != "" || usuario != null){
			cambia('home');
		}
		inicio();
	}
}

window.history.clear = function(){
	location.hash = "#";
	this.go(-(this.length-1));
}

function cambia(id){
	var list = document.getElementsByClassName("tab-item");
	for (var i = 0; i < list.length; i++) {

		if(list[i].id == id){
			list[i].className = "tab-item active";
		}
		else{
			list[i].className = "tab-item";
		}
	}
}

function cambia2(hash){
	var url = window.location.hash;
	if(url == ""){
		window.location.hash = hash;
	} else {
		window.location.replace(hash);
	}
}

function guardarsesion(user, wel){
	localStorage.usuario = user;
	localStorage.welcome = wel;
	window.history.clear();
}

function cuales(es){
	var day = ""
	switch (es) {
		case "CVZLAHOME":
		day = "home";
		break;
		case "CVZLAPUBLISH":
		day = "publicaciones";
		break;
		case "CVZLANTFC":
		day = "notificaciones";
		break;
		case "CVZLACHAT":
		day = "chat";
		break;
		case "CVZLAPERFIL":
		day = "perfil";
		break;
	}
	return day;
}

function actualizadatos(){
	var usuario =  localStorage.getItem("usuario");
	var genero = document.getElementById("genero").value;
	var fnacimiento = document.getElementsByName('fnacimiento')[0].value + '-' + document.getElementsByName('fnacimiento')[1].value + '-' + document.getElementsByName('fnacimiento')[2].value;
	var cedula = document.getElementById('cedula').value;
	var telefono = document.getElementById('telefono').value;
	var profesion = document.getElementById('profesion').value;
	var direccion = document.getElementById('direccion').value;
	var zipcode = document.getElementsByName('zcode')[0].value + document.getElementsByName('zcode')[1].value + document.getElementsByName('zcode')[2].value + document.getElementsByName('zcode')[3].value + document.getElementsByName('zcode')[4].value;
	var recomendado = document.getElementById('recomendado').value;
	var ciudad = document.getElementById('ciudad').value;
	var estado = document.getElementById("estado").options[document.getElementById("estado").selectedIndex].text;
	var vReturn = uniface.getInstance('CVZLADATOS').activate('update', usuario, genero, fnacimiento, cedula, telefono, profesion, direccion, zipcode, recomendado, ciudad, estado).then(function (response) {
		actualizardata(response.returnValue);
	}).catch(function (err) {
		console.log(err);
	});
}

function actualizardata(undato){
	if(undato == "2"){
		window.location.hash = '#CVZLAWELCOME';
	} 
}

function publicar(){
	var contenido = document.getElementsByName('contenido')[0].value;
	var titulo = document.getElementsByName('titulo')[0].value;
	var file = document.getElementById("fileA").files[0];
	if(titulo == "" || contenido == ""){
		uniface.activate('mensaje', 'Complete los campos');
	} else if(file){
		uploadFile();
	} else {
		var d = fecha();
		uniface.getInstance('CVZLAPUBLICAR').activate('publicar', titulo, contenido, d, "", "");
		window.history.back();
	}
}

function preview(){
	var file = document.getElementById("fileA").files[0];
	var reader  = new FileReader();
	var preview = document.getElementsByClassName('preview')[0];

	reader.onloadend = function () {
		preview.src = reader.result;
	}
	
	var limpia = function(){
		preview.src = "";
		document.getElementById("fileA").value = '';
		document.getElementById("archivo").style.display="none";
		document.getElementsByClassName("archivo")[0].style.display = 'none';
		document.getElementById("fileName").innerHTML = '';
	}

	if (file) {
		var res = file.type;
		var tipo = res.split("/");
		if(file.size >= 1000000){
			limpia();
			uniface.activate("mensaje", "El tamaño del archivo es mayor a 1mb");
		} else if(tipo[0] == 'image'){
			reader.readAsDataURL(file);
		} else if(file.type == "text/plain" || file.type == "application/pdf" || file.type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document"){
			preview.src = "";
			document.getElementById("archivo").style.display="block";
			document.getElementsByClassName("archivo")[0].style.display = 'block';
			document.getElementById("fileName").innerHTML = file.name;
		} else {
			limpia();
			uniface.activate('mensaje', 'tipo de archivo no soportado');
		}
	} else {
		limpia();
	}
}

function uploadFile(){

	var file = document.getElementById("fileA").files[0];
	var res = file.type;
	var tipo = res.split("/");
	var imagen = '';
	var documento = '';
	
	if (!file) {
		alert("No file selected!");
		return;
	}
	
	if(tipo[0] == 'image'){
		imagen = file.name;
	} else if(file.type == "text/plain" || file.type == "application/pdf" || file.type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document"){
		documento = file.name;
	}

	var d = new Date();
	var formData = new FormData();
	client = new XMLHttpRequest();

	formData.append("FILE_UPLOAD.DUMMY.1", file);
	storeFileName = file.name;
	formData.append("UPLOADFILENAME.DUMMY.1", storeFileName);

	client.onerror = function (e) {
		alert("onError");
	};

	client.onload = function (e) {
		var titulo = document.getElementsByName('titulo')[0].value;
		var contenido = document.getElementsByName('contenido')[0].value;
		var d = fecha();
		uniface.getInstance('CVZLAPUBLICAR').activate('publicar', titulo, contenido, d, imagen, documento);
		document.getElementById("fileA").value = "";
		window.history.back();
	};

	client.onabort = function (e) {
		alert("Upload aborted");
	};

	client.open("POST", "FILE_UPLOAD_USPP");
	client.send(formData);
}

function fecha(){

	function makeArray() {
		for (i = 0; i<makeArray.arguments.length; i++){
			this[i + 1] = makeArray.arguments[i];
		}
	}
	var months = new makeArray('Enero','Febrero','Marzo','Abril','Mayo',
		'Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
	var date = new Date();
	var day = date.getDate();
	var month = date.getMonth() + 1;
	var yy = date.getYear();
	var year = (yy < 1000) ? yy + 1900 : yy;
	var fecha = day + " de " + months[month] + " de " + year + " " +date.getHours() + ":"+date.getMinutes()
	return fecha;
}

function comentar(){
	var usuario =  localStorage.getItem("usuario");
	var contenido = document.getElementsByClassName("comentarios")[0].value;
	if(contenido == ""){
		uniface.activate('mensaje', 'Ingrese contenido');
	} else {
		uniface.getInstance('CVZLAPOST').activate('comentar', usuario, contenido);
	}
}

function cerrarsesion(){
	uniface.getInstance('CVZLAHOME').remove();
	localStorage.clear();
	window.history.clear();
}

function video(){
	if(document.getElementById('aprobado').value.toLowerCase() == 'aprobado'){
		var video = document.getElementsByTagName('video')[0];
		var sources = video.getElementsByTagName('source');
		var str = sources[0].src;

		var res = str.split("/");
		console.log(res);
		if(res[7] == 'video3.mp4'){
			document.getElementById("capa").className = "backdrop visible active";
			document.getElementById("popup").className = "popup-container popup-showing active";
		} else {
			videos = videos+1;
			var src2 = '../css/img/video/video'+videos+'.mp4';
			sources[0].src = src2;
			video.load();
			document.getElementsByName('video')[0].disabled=true; 
			document.getElementById('aprobado').disabled=true;
			document.getElementById('aprobado').value="";
		}
	} else { 
		alert('Debe colocar la palabra APROBADO')
	}
}

function cambiovideo(){
	document.getElementsByName('video')[0].disabled=false; 
	document.getElementById('aprobado').disabled=false;
}

function modaleventos(imagen){
	var modal = document.getElementById('myModal');
	var img = document.getElementById('myImg');
	var modalImg = document.getElementById("img01");
	modal.style.display = "block";
	modalImg.src = imagen.src;
}

function show(caso){
	if(caso == "true"){
		document.getElementsByClassName("backdrop")[0].className = "backdrop visible active";
		document.getElementsByClassName("popup-container")[0].className = "popup-container popup-showing active";
	} else if(caso == "false"){
		document.getElementsByClassName("backdrop")[0].className = "backdrop";
		document.getElementsByClassName("popup-container")[0].className = "popup-container";
	}
}

function ejecutar(idelemento){
	var aux = document.createElement("div");
	aux.setAttribute("contentEditable", true);
	var contenido2 = idelemento.offsetParent.querySelector(".contenido");
	var texto = "";
	var linck = "";
	var compartir = "";
	if(contenido2.children.length == 0){
		texto = contenido2.innerText;
	} else {
		texto = contenido2.innerText;
		var enlace = idelemento.offsetParent.querySelector(".contenido")
		for(var i=0;i<enlace.children.length; i++){
			if(enlace.children[i].nodeName == "A"){
				linck = enlace.children[i].href;
			}
		}
	}

	if(linck == ""){
		compartir = texto;
	}else{
		compartir = texto + " " + linck;
	}

	aux.innerHTML = compartir;
	aux.setAttribute("onfocus", "document.execCommand('selectAll',false,null)"); 
	document.body.appendChild(aux);
	aux.focus();
	document.execCommand("copy");
	document.body.removeChild(aux);

	uniface.getInstance("CVZLAEVENTOS").activate("mensajes", "Compartido al portapapeles")

}

uf_clientside.CVZLAHOME = { 
	"alert" : function(msg) {
		console.log(msg);
	}
};

uf_clientside.CVZLAINFO = { 
	"busqueda" : function() {
		var dsp = uniface.getInstance('CVZLABUSQUEDA');
		if(dsp != null){
			uniface.getInstance('CVZLABUSQUEDA').remove();
		}
	},
	"showclose" : function() {
		show('false');
	}
};

uf_clientside.CVZLAPUBLICIDAD = { 
	"busqueda" : function() {
		var dsp = uniface.getInstance('CVZLABUSQUEDA');
		if(dsp != null){
			uniface.getInstance('CVZLABUSQUEDA').remove();
		}
	},
	"showclose" : function() {
		show('false');
	}
};

uf_clientside.CVZLAWELCOME = { 
	"pasar" : function() {
		window.history.clear();
	}
};

uf_clientside.CVZLAPERFIL = { 
	"showclose" : function() {
		show('false');
	}
};

uf_clientside.CVZLASPONSORS = { 
	"muestra" : function() {
		show('true');
	}
};

function galeria(signo){
	var j = 0;
	var imagenes = document.getElementsByClassName('col');
	for(var i = 0; i<imagenes.length; i++){
		if(imagenes[i].style.display=='block'){
			if(signo==1){
				j = i+1;
			}else{
				j = i-1;
			}
			if(j<=imagenes.length-1 && j>=0){
				imagenes[j].style.display='block';
				imagenes[i].style.display='none';
				break;
			}
		}
	}
}